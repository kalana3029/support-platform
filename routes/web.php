<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {

    if (auth()->check()) {
        return redirect()->route('home');
    } else {
        return view('welcome');
    }
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tickets/new', 'TicketController@index')->name('ticket-new');
Route::post('/tickets/new', 'TicketController@create')->name('ticket-new-store');
Route::get('/tickets/{reference}', 'TicketController@show')->name('ticket-new-show');
Route::post('/tickets/{reference}', 'TicketController@replyTicket')->name('ticket-new-replyTicket');

Route::group(['prefix' => 'ajax'], function() {
    Route::patch('/tickets/{reference}', 'Ajax\ApiController@updateTicket')->name('ajax.ticket.update');
    Route::get('/tickets/search', 'Ajax\ApiController@searchTicket')->name('ajax.ticket.search');
});