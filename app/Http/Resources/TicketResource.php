<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $ticket_status = config('constants.ticket_status');
        return [
            'id' => $this->id,
            'reference' => $this->reference,
            'status' => $ticket_status[$this->status],
            'customer' => [
                'name' => $this->user->name
            ]
        ];
    }
}
