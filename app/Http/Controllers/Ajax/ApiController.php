<?php

namespace App\Http\Controllers\Ajax;

use Exception;
use App\Services\TicketService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\TicketResource;

class ApiController extends Controller
{

    private $ticket_service;

    /**
     * Create a new repository instance.
     * 
     * @param User $user
     */
    public function __construct(TicketService $ticket_service)
    {
        $this->middleware('auth');
        $this->ticket_service = $ticket_service;
    }

    public function updateTicket($reference,Request $request)
    {
        try {
            
            $ticket_data = $request->all();
            
            $ticket = $this->ticket_service->update($reference, $ticket_data);
            
            $data = [
                'status' => true,
                'data' => [
                    'ticket' => $ticket
                ],
                'message' => 'updated succesfully'
            ];
            
            return response()->json($data,200);
        } catch (\Exception $ex) {
            
            $data = [
                'status' => false,
                'data' => [],
                'message' => $ex->getMessage()
            ];
            
           return response()->json($data,200);
            
        }
    }
    
    public function searchTicket(Request $request)
    {
        try {
            
            $keyword = $request->get('search');
            $filter_by = $request->get('filter_by');
            $tickets = TicketResource::collection($this->ticket_service->search($keyword,$filter_by))->toArray($request);
            $data = [
                'status' => true,
                'data' => [
                    'tickets' => $tickets
                ],
                'message' => 'listed succesfully'
            ];
            
            return response()->json($data,200);
        } catch (\Exception $ex) {
            
            $data = [
                'status' => false,
                'data' => [],
                'message' => $ex->getMessage()
            ];
            
           return response()->json($data,200);
            
        }
    }

}
