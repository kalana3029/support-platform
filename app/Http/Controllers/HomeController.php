<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Services\TicketService;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class HomeController extends Controller
{

    protected $ticket_service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TicketService $ticket_service)
    {
        $this->ticket_service = $ticket_service;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $data = [];
        if ($user->isAgent()) {
            $data['tickets'] = $this->paginate($this->ticket_service->all());
            $data['ticket_status'] = config('constants.ticket_status');
            return view('agent-home', $data);
        } else {
            return view('home', $data);
        }
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

}
