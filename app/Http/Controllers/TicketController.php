<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Services\TicketService;
use App\Services\UserService;
use App\Http\Requests\StoreGuestTicketRequest;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;
use App\Http\Requests\StoreTicketMessageRequest;
use App\Mailers\TicketMailer;

class TicketController extends Controller
{

    use RegistersUsers;

    protected $ticket_service;
    protected $user_service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TicketService $ticket_service, UserService $user_service)
    {
        $this->ticket_service = $ticket_service;
        $this->user_service = $user_service;
    }

    public function index()
    {

        $ticket_data = [
            'name' => '',
            'email' => '',
            'phone' => '',
        ];

        if (Auth::check()) {
            $ticket_data = [
                'name' => Auth::user()->name,
                'email' => Auth::user()->email,
                'phone' => Auth::user()->phone,
            ];
        }

        $data['ticket_data'] = $ticket_data;

        return view('ticket.ticket-new', $data);
    }

    public function create(StoreGuestTicketRequest $request,TicketMailer $mailer)
    {
        try {

            if (!Auth::check()) {
                $user_data = $request->except(['description']);
                $user_data['role_id'] = 2;
                $user_data['password'] = Str::random(10);
                $user = $this->user_service->create($user_data);
                $mailer->sendLoginDetailsMail($user);
            } else {
                $user = Auth::user();
            }

            $ticker_data = $request->except(['name', 'email', 'phone']);
            $ticker_data['reference'] = strtolower(Str::random(6));
            $ticket = $user->tickets()->create($ticker_data);
            $mailer->sendTicketMail($user,$this->ticket_service->findByRef($ticket->reference));
            $this->guard()->login($user);

            return redirect()->route('home')->with('success', 'Your message has been sent successfully!');
        } catch (Exception $ex) {
            dd($ex);
             return redirect()->route('ticket-new')->with('error', 'Unable to send right now!');
        }
    }

    public function show($reference)
    {
        $ticket = $this->ticket_service->findByRef($reference);
        
        $ticket->ticketMessages()->update(['status' => 1]);

        $data['ticket'] = $ticket;
        $data['ticket_status'] = config('constants.ticket_status');
        return view('ticket.ticket-show', $data);
    }

    public function replyTicket($reference, StoreTicketMessageRequest $request)
    {
        try {
            $ticket = $this->ticket_service->findByRef($reference);
            $ticket_message = $request->all();
            $ticket_message['user_id'] = Auth::user()->id;
            $ticket->ticketMessages()->create($ticket_message);

            return redirect()->route('ticket-new-show', ['reference' => $reference])
                    ->with('success', 'Your message has been sent successfully!');;
        } catch (Exception $ex) {
             return redirect()->route('ticket-new-show', ['reference' => $reference])->with('error', 'Unable to send right now!');
        }
    }

}
