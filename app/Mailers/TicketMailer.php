<?php

namespace App\Mailers;

use App\Ticket;
use Illuminate\Contracts\Mail\Mailer;

class TicketMailer
{

    protected $mailer;
    protected $from_address = '';
    protected $from_name = '';
    protected $to;
    protected $subject;
    protected $view;
    protected $data = [];

    public function __construct(Mailer $mailer)
    {
        $this->from_address = config('mail.address');
        $this->from_name = config('mail.name');
        $this->mailer = $mailer;
    }

    public function sendTicketMail($user, Ticket $ticket)
    {
        $ticket_status = config('constants.ticket_status');
        $this->to = $user->email;
        $this->subject = "[Ticket Ref: $ticket->reference]";
        $this->view = 'emails.ticket_info';
        $this->data = compact('user', 'ticket','ticket_status');

        return $this->deliver();
    }
    
    public function sendLoginDetailsMail($user)
    {
        $this->to = $user->email;
        $this->subject = "[User : $user->email]";
        $this->view = 'emails.login_info';
        $this->data = compact('user');

        return $this->deliver();
    }

    public function deliver()
    {
        $this->mailer->send($this->view, $this->data, function($message) {
            $message->from($this->from_address, $this->from_name)
                    ->to($this->to)->subject($this->subject);
        });
    }

}
