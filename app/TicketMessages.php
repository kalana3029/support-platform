<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketMessages extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ticket_id',
        'user_id',
        'message',
        'status'
    ];

    /**
     * Get the user that owns the ticket.
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
