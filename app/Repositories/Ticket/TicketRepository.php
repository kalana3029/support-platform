<?php

namespace App\Repositories\Ticket;

/**
 * Description of TicketRepository
 *
 */
use App\Ticket;
use App\Repositories\Ticket\TicketRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TicketRepository implements TicketRepositoryInterface
{

    protected $ticket;

    /**
     * Create a new repository instance.
     * 
     * @param Ticket $ticket
     */
    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }

    public function findByRef($reference)
    {
        $model = $this->ticket->where('reference', $reference)->first();
        if (empty($model)) {
            throw new ModelNotFoundException;
        }
        return $model;
    }

    public function create(array $data)
    {
        return $this->ticket->create($data);
    }

    public function all()
    {
        return $this->ticket->all();
    }

    public function update($reference, array $data)
    {
        $ticket = $this->findByRef($reference);
        $ticket->update($data);

        return $ticket;
    }

    public function search($keyword, $filter_by)
    {
        $query = $this->ticket->with('user');

        if ($filter_by == 'reference') {
            $query->where("reference", "=", $keyword);
        }

        if ($filter_by == 'customer_name') {
            $query->whereHas('user', function ($query) use($keyword) {
                $query->where("name", "LIKE", "%$keyword%");
            });
        }

        return $query->get();
    }

}
