<?php

namespace App\Repositories\Ticket;

use App\Ticket;

interface TicketRepositoryInterface
{
    public function all();
    
    public function findByRef($reference);
    
    public function create(array $data);
    
    public function update($reference,array $data);
    
    public function search($keyword,$filter_by);
}
