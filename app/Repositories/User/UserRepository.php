<?php

namespace App\Repositories\User;

/**
 * Description of UserRepository
 *
 */
use App\User;
use App\Repositories\User\UserRepositoryInterface;
use App\InventorySourceUser;

class UserRepository implements UserRepositoryInterface
{

    protected $user;

    /**
     * Create a new repository instance.
     * 
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function create(array $data)
    {
        return $this->user->create($data);
    }

}
