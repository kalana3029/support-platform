<?php

namespace App\Repositories\User;

use App\User;

interface UserRepositoryInterface
{
    public function create(array $data);
}
