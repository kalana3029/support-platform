<?php

namespace App\Services;

use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Ticket\TicketRepositoryInterface;
use App\Http\Requests\StoreGuestTicketRequest;

/**
 * Description of TicketService
 *
 */
class TicketService
{

    /**
     * The user repository implementation.
     *
     * @var TicketRepositoryInterface
     */
    protected $ticket;

    /**
     * Create a new controller instance.
     *
     * @param  TicketRepositoryInterface  $ticket
     * @return void
     */
    public function __construct(TicketRepositoryInterface $ticket)
    {
        $this->ticket = $ticket;
    }

    public function findByRef($reference)
    {
        return $this->ticket->findByRef($reference);
    }

    public function create(array $data)
    {
        return $this->ticket->create($data);
    }

    public function all()
    {
        return $this->ticket->all();
    }

    public function getTicketsByAgent($agent)
    {
        return $this->all()->where('assign_by', $agent);
    }

    public function update($reference, array $data)
    {

        return $this->ticket->update($reference, $data);
    }

    public function search($keyword,$filter_by)
    {
        return $this->ticket->search($keyword,$filter_by);
    }

}
