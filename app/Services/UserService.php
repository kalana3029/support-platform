<?php

namespace App\Services;

use App\Repositories\User\UserRepositoryInterface;

/**
 * Description of UserService
 *
 */
class UserService
{

    /**
     * The user repository implementation.
     *
     * @var UserRepositoryInterface
     */
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepositoryInterface  $users
     * @return void
     */
    public function __construct(UserRepositoryInterface $users)
    {
        $this->users = $users;
    }

    public function create(array $data)
    {
        return $this->users->create($data);
    }


}
