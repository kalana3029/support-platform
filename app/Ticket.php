<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'reference',
        'description',
        'status',
        'assign_by'
    ];

    public function setReferenceAttribute($value)
    {
        $this->attributes['reference'] = date('YmdHis') . $value;
    }

    /**
     * Get the user that owns the ticket.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ticketMessages()
    {
        return $this->hasMany(TicketMessages::class);
    }

    public function isAssinged()
    {
        return $this->assign_by != null;
    }

    public function assignBy()
    {
        if ($this->isAssinged()) {
            return $this->belongsTo(User::class, 'assign_by', 'id');
        }
        return null;
    }

}
