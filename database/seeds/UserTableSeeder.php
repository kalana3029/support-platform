<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = DB::table('users')->where('email', 'support@sample.com')->first();

        if (empty($user)) {
            DB::table('users')->insert([
                'name' => 'Support Agent',
                'email' => 'support@sample.com',
                'password' => Hash::make('support123'),
                'role_id' => 1
            ]);
        }
        
        $user = DB::table('users')->where('email', 'user@sample.com')->first();

        if (empty($user)) {
            DB::table('users')->insert([
                'name' => 'Sample User',
                'email' => 'user@sample.com',
                'password' => Hash::make('user123'),
                'role_id' => 2
            ]);
        }
    }

}
