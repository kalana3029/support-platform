<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = DB::table('users')->find(1);

        if (empty($role)) {
            DB::table('roles')->insert([
                'id' => 1,
                'name' => 'support_agent',
                'display_name' => 'Support Agent'
            ]);
        }

        $role2 = DB::table('users')->find(2);

        if (empty($role2)) {
            DB::table('roles')->insert([
                'id' => 2,
                'name' => 'customer',
                'display_name' => 'Customer'
            ]);
        }
    }

}
