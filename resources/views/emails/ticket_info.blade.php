<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Support Ticket Details</title>
</head>
<body>
    <p>
        Thank you {{ ucfirst($user->name) }} for contacting our support team. A support ticket has been opened for you. You will be notified when a response is made by email. The details of your ticket are shown below:
    </p>

    <p>Reference: {{ $ticket->reference }}</p>
    <p>Status: {{ $ticket_status[$ticket->status]['title'] }}</p>

    <p>
        You can view the ticket at any time at {{ url('tickets/'. $ticket->reference) }}
    </p>

</body>
</html>