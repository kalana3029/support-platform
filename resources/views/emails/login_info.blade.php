<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User Login Details</title>
</head>
<body>
    <p>
        Thank you {{ ucfirst($user->name) }} for connecting our support team.</p>

    <p>Username: {{ $user->email }}</p>
    <p>Password: {{ $user->password }}</p>

</body>
</html>