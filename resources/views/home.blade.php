@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-2 col-md-6" type="search" placeholder="Ticket Ref." aria-label="Search" onkeyup="searchTicket(this)">
                                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="ticket-table" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">#Ticket Ref.</th>
                                        <th scope="col">Customer</th>
                                        <th scope="col">Ticket Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>
    function searchTicket(event) {

        var data = {};
        data['_token'] = "{{ csrf_token() }}";
        data['search'] = event.value;
        data['filter_by'] = 'reference';
        $.ajax({
            url: "/ajax/tickets/search",
            type: 'get',
            data: data,
            dataType: 'json',
            success: function (response) {

                var data = response.data;

                var html = '';
                $.each(data.tickets, function (index, value) {
                    console.log(value);
                    html += "<tr>\n\
                                <th scope='row'>" + value.reference + "</th>\n\
                                <th scope='row'>" + value.customer.name + "</th>\n\
                                <th scope='row'><span class='badge badge-" + value.status.class + "'>" + value.status.title + "</span></th>\n\
                                <td><a class='btn btn-secondary btn-sm' href='tickets/" + value.reference + "'>View</a></td><tr>";
                });

                $("#ticket-table tbody").html(html);
            }
        });
    }

</script>
@endpush