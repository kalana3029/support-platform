@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="alert alert-{{$ticket_status[$ticket->status]['class']}}" role="alert">
                Ticket Status : {{$ticket_status[$ticket->status]['title']}}
            </div>
            <div class="media">
                <img src="{{asset('img/user.png')}}" class="align-self-start mr-3" alt="..." style="width: 50px;height: 50px">
                <div class="media-body">
                    <h5 class="mt-0">{{$ticket->user->name}}</h5>
                    {{$ticket->description}}

                    @foreach($ticket->ticketMessages as $message)
                    <div class="media mt-3">
                        <a class="mr-3" href="#">
                            @if($message->user->role->id === 1)
                            <img src="{{asset('img/agent.png')}}" class="mr-3" alt="..." style="width: 50px;height: 50px">
                            @else
                            <img src="{{asset('img/user.png')}}" class="align-self-start mr-3" alt="..." style="width: 50px;height: 50px">
                            @endif
                        </a>
                        <div class="media-body">
                            <h5 class="mt-0">{{$message->user->name}}</h5>
                            {{$message->message}}
                        </div>
                    </div>
                    @endforeach

                    <div class="media mt-3">
                        @if(auth()->user()->role->id === 1)
                        <img src="{{asset('img/agent.png')}}" class="mr-3" alt="..." style="width: 50px;height: 50px">
                        @else
                        <img src="{{asset('img/user.png')}}" class="align-self-start mr-3" alt="..." style="width: 50px;height: 50px">
                        @endif
                        <div class="media-body">
                            <form method="POST" action="{{ route('ticket-new-replyTicket',['reference' => $ticket->reference ]) }}">
                                @csrf

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <textarea class="form-control" id="message" name="message" rows="5" required></textarea>
                                        @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Reply') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4" >
            <div class="card">
                @if($ticket->isAssinged())
                <div class="card-header">
                    Agent name : {{$ticket->assignBy->name}}
                </div>
                @endif
                <div class="card-body">
                    @if(auth()->user()->role->id === 1)
                    <form>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-6 col-form-label">Ticket Status</label>
                            <div class="col-md-6">
                                <select class="form-control form-control-sm" onchange="changeTicketStatus(this)">
                                    @foreach($ticket_status as $key => $status)
                                    <option value="{{$key}}" {{$key == $ticket->status ? 'selected' : ''}}>{{$status['title']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @if(!$ticket->isAssinged())
                        <button type="submit" class="btn btn-primary mb-2" data-user="{{auth()->user()->id}}" onclick="assignToMe(this)">Assign to me</button>
                         @endif
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    function changeTicketStatus(event) {

        var data = {};
        data['_token'] = "{{ csrf_token() }}";
        data['status'] = event.value;
        $.ajax({
            url: "/ajax/tickets/{{$ticket->reference}}",
            type: 'patch',
            data: data,
            dataType: 'json',
            success: function (response) {
                location.reload();
            }
        });
    }
    
    function assignToMe(event) {

        var data = {};
        data['_token'] = "{{ csrf_token() }}";
        data['assign_by'] = $(event).data('user');
        data['status'] = 1;
        $.ajax({
            url: "/ajax/tickets/{{$ticket->reference}}",
            type: 'patch',
            data: data,
            dataType: 'json',
            success: function (response) {
                location.reload();
            }
        });
    }
</script>
@endpush
