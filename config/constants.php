<?php

return [
    'ticket_status' => [
        0 => ['title' => 'pending','class' =>'danger'],
        1 => ['title' => 'on Going','class' =>'primary'],
        2 => ['title' => 'Done','class' => 'success'],
    ]
];

